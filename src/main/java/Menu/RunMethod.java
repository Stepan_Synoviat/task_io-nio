package Menu;

import java.io.IOException;

public interface RunMethod {
	void start() throws IOException, ClassNotFoundException;
}
