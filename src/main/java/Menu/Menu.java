package Menu;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task1_serializable.RunTask1;
import task2_compare_performance.ComparePerformance;
import task4_displayAllTheComments.CommentReader;
import task5_DirectoryContent.DirectoryContent;


public final class Menu {
	private Map<String, String> menu;
	private Map<String, RunMethod> methodsMenu;
	private static final Scanner scanner = new Scanner(System.in);
	private static final Logger loger = LogManager.getLogger(Menu.class);

	private Menu() {
		menuStructure();
	}

	private void menuStructure() {
		menu = new LinkedHashMap<>();
		methodsMenu = new LinkedHashMap<>();

		menu.put("1", " 1 - Serializable.");
		menu.put("2", " 2 - Compare performance of reading and writing files ");
		menu.put("3", " 3 - Read a Java source code file.");
		menu.put("4", " 4 - Get contents directories.");
		//method doesn`t exist
		//menu.put("5", " 5 - Server/Client example.");
		menu.put("0", " 0 - Exit.");

		methodsMenu.put("1", RunTask1::runTaskSerialization);
		methodsMenu.put("2", ComparePerformance::runTaskComparePerformance);
		methodsMenu.put("3", CommentReader::showComments);
		methodsMenu.put("4", DirectoryContent::runDirectoryContent);
		//method doesn`t exist
		//methodsMenu.put("5", Server::runNIO);

		methodsMenu.put("0", this::exit);
	}

	public static void showMenu() {
		String choice;
		Menu menu = new Menu();
		while (true) {
			try {
				System.out.println("\n");
				menu.show();
				if (loger.isInfoEnabled()) {
					loger.info("Choose something ");
				}
				choice = scanner.nextLine();
				System.out.println("\n\n");
				menu.methodsMenu.get(choice).start();
			} catch (IOException e) {
				if (loger.isErrorEnabled()) {
					loger.error(e.getMessage());
				}
			} catch (Exception e) {
				if (loger.isFatalEnabled()) {
					loger.fatal(e.getMessage());
				}
			}
		}
	}

	private void show() {
		loger.info("Menu");
		for (String string : menu.values()) {
			loger.info(string);
		}
	}

	private void exit() {
		System.exit(0);
	}
}

