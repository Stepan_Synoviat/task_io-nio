package task1_serializable;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class RunTask1
 */
public class RunTask1 {
	/**
	 * Logger initialization for RunTask1.class
	 */
	static final Logger userLogger = LogManager.getLogger(RunTask1.class);
	/**
	 * path to serializable file
	 */
	private static final String PATH_NAME = "C:\\Users\\Stepan Synoviat\\Documents\\task_IONIO\\src\\main\\resources\\ship.txt";

	/**
	 * function to serializable object
	 *
	 * @param obj - object which will be serializabled
	 * @throws IOException
	 */
	private static void serializable(Object obj) throws IOException {
		try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
				new FileOutputStream(PATH_NAME));) {
			objectOutputStream.writeObject(obj);
			objectOutputStream.flush();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * function to deserializable object
	 *
	 * @param filePath
	 * @return Ship
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private static Object deserializable(String filePath)
			throws IOException, ClassNotFoundException {
		try (ObjectInputStream objectInputStream = new ObjectInputStream(
				new FileInputStream(filePath));) {
			return objectInputStream.readObject();
		}
	}

	/**
	 * function runs serializable and deserializable methods
	 *
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static void runTaskSerialization() throws IOException, ClassNotFoundException {

		Droid droid1 = new Droid("R2D2", 1, 100);
		Droid droid2 = new Droid("R2D3", 2, 100);
		Ship ship = new Ship("Argo", 1, "find the golden fleece", new ArrayList<>(Arrays.asList(droid1, droid2)));
		userLogger.info("Before serialization " + ship.toString());
		try {
			serializable(ship);
			Ship ship1 = (Ship) deserializable(PATH_NAME);
			userLogger.info("After serialization " + ship1.toString());
		} catch (IOException e) {
			userLogger.error("This is IO exception");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			userLogger.error("This is Class not found exception");
			e.printStackTrace();
		}
	}
}

