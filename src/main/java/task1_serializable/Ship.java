package task1_serializable;

import java.io.Serializable;
import java.util.List;

/**
 * Class Ship with parameters <b>name</b> <b>model</b> <b>mission</b> <b>list</b>
 * @author Stepan Synoviat
 * @version 1.0
 */
public class Ship implements Serializable {
	/**
	 *field name of a ship instance
	 */
	private String name;
	/**
	 * field model of a ship instance
	 */
	private int model;
	/**
	 * field mission of a ship instance
	 */
	private transient String mission;
	/**
	 * field list of droid instances
	 */
	private List<Droid> list;

	/**
	 * Constructor - create a new instance
	 * @see Ship#Ship(String, int, String, List)
	 */
	public Ship(final String name, final int model, final String mision, final List<Droid> list) {
		this.name = name;
		this.model = model;
		this.mission = mision;
		this.list = list;
	}

	public String toString() {
		return "Ship{" + "name='"
				+ name + '\''
				+ ", model=" + model
				+ ", mission=" + mission
				+ ", list='" + list + '\''
				+ '}';
	}
}
