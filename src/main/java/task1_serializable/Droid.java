package task1_serializable;

import java.io.Serializable;

/**
 * Class Droid with parameters <b>name</b> <b>id</b> <b>power</b>
 *  * @author Stepan Synoviat
 *  * @version 1.0
 */
public class Droid implements Serializable {
	/**
	 *field name of a droid instance
	 */
	private String name;
	/**
	 *field id of a droid instance
	 */
	private int id;
	/**
	 *field power of a droid instance
	 */
	private int power;

	/**
	 * Constructor - create a new instance
	 * @see Droid#Droid(String, int, int)
	 */
	public Droid(String name, int id, int power) {
		this.name = name;
		this.id = id;
		this.power = power;
	}

	/**
	 * function to get field value{@link Droid#name}
	 * @return name of a droid instance
	 */
	public String getName() {
		return name;
	}

	/**
	 * name determination procedure {@link Droid#name}
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * function to get field value{@link Droid#id}
	 * @return id of a droid instance
	 */
	public int getId() {
		return id;
	}
	/**
	 * name determination procedure {@link Droid#id}
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * function to get field value{@link Droid#power}
	 * @return power of a droid instance
	 */
	public int getPower() {
		return power;
	}
	/**
	 * name determination procedure {@link Droid#power}
	 * @param power
	 */
	public void setPower(int power) {
		this.power = power;
	}

	public String toString() {
		return "Droid{" + "name='"
				+ name + '\''
				+ ", id=" + id
				+ ", power=" + power + '\''
				+ '}';
	}
}
