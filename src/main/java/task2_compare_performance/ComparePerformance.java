package task2_compare_performance;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

/**
 * Class ComparePerformance
 */
public class ComparePerformance {
	/**
	 * Logger initialization for ComparePerformance.class
	 */
	static final Logger userLogger = LogManager.getLogger(ComparePerformance.class);
	/**
	 * field path to file which will be read and written
	 */
	private static final String PATH_NAME = "C:\\Users\\Stepan Synoviat\\Documents\\task_IONIO\\JavaKid8x11_ru.pdf";
	/**
	 * field default value of Buffer size
	 */
	private static final int defaultBufferSize = 100 * 1024;

	/**
	 * function tests input without buffer
	 *
	 * @param fileName
	 */
	public static void testUsualInput(String fileName) {
		try (InputStream inpt = new FileInputStream(fileName)) {
			long timeOfBegining = System.currentTimeMillis();
			userLogger.info(timeOfBegining);
			int data = inpt.read();
			while (data != -1) {
				data = inpt.read();
			}
			long timeOfEnding = System.currentTimeMillis();
			userLogger.info(timeOfEnding);
			long difference = timeOfEnding - timeOfBegining;
			double seconds = (double) difference;
			userLogger.info(seconds);
		} catch (FileNotFoundException e) {
			userLogger.error("This is File not found message");
			e.printStackTrace();
		} catch (IOException e) {
			userLogger.error("This is IO exception message");
			e.printStackTrace();
		}
	}

	/**
	 * function tests input with buffer
	 *
	 * @param fileName
	 */
	public static void testBufferedlInput(String fileName) {
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));) {
			long timeOfBeggining = System.currentTimeMillis();
			userLogger.info(timeOfBeggining);
			int data = bufferedReader.read();
			while (data != -1) {
				data = bufferedReader.read();
			}
			long timeOfEnding = System.currentTimeMillis();
			userLogger.info(timeOfEnding);
			long difference = timeOfEnding - timeOfBeggining;
			double seconds = (double) difference;
			userLogger.info(seconds);
		} catch (FileNotFoundException e) {
			userLogger.error("This is File not found message");
			e.printStackTrace();
		} catch (IOException e) {
			userLogger.error("This is IO exception message");
			e.printStackTrace();
		}
	}

	/**
	 * function tests output without buffer
	 *
	 * @param fileName
	 */
	public static void testUsualOutput(String fileName, String someText) {
		try (OutputStream output = new FileOutputStream(fileName)) {
			long timeOfBeggining = System.nanoTime();
			userLogger.info(timeOfBeggining);
			output.write(someText.getBytes());
			long timeOfEnding = System.nanoTime();
			userLogger.info(timeOfEnding);

			long difference = timeOfEnding - timeOfBeggining;
			//System.out.println(elapsedTime2);
			double seconds = (double) difference;
			userLogger.info(seconds);
			//	log.info("Time processing: " + seconds + " seconds");

		} catch (IOException e) {
			userLogger.error("This is IO exception message");
			e.printStackTrace();
		}
	}

	/**
	 * function tests output with buffer
	 *
	 * @param fileName
	 */
	public static void testBufferdOutput(String fileName, String someText, int buffersize) {
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			BufferedOutputStream bos = new BufferedOutputStream(fos, buffersize);
			DataOutputStream dos = new DataOutputStream(bos);
			long timeOfBegining = System.nanoTime();
			userLogger.info(timeOfBegining);
			dos.write(someText.getBytes());
			long timeOfEnding = System.nanoTime();
			userLogger.info(timeOfEnding);

			long difference = timeOfEnding - timeOfBegining;
			//System.out.println(elapsedTime2);
			double seconds = (double) difference;
			userLogger.info(seconds);
			//	log.info("Time processing: " + seconds + " seconds");

		} catch (IOException e) {
			userLogger.error("This is IO exception message");
			e.printStackTrace();
		}
	}

	public static void runTaskComparePerformance() {
		ComparePerformance.testUsualInput(PATH_NAME);
		ComparePerformance.testBufferedlInput(PATH_NAME);
		ComparePerformance.testUsualOutput(PATH_NAME, "someText");
		ComparePerformance.testBufferdOutput(PATH_NAME, "Some text", defaultBufferSize);
	}
}
