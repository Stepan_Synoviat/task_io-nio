import Menu.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task2_compare_performance.ComparePerformance;

public class Main {
	static final Logger userLogger = LogManager.getLogger(ComparePerformance.class);

	public static void main(String[] args) {
		userLogger.debug("let run the program");
		Menu.showMenu();
	}
}
