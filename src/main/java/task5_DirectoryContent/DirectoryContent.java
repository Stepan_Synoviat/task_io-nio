package task5_DirectoryContent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task4_displayAllTheComments.CommentReader;

import java.io.File;
import java.io.FileNotFoundException;

public class DirectoryContent {
	/**
	 * Logger initialization for CommentReader.class
	 */
	private static Logger logger = LogManager.getLogger(CommentReader.class);
	/**
	 * path to file or directory
	 */
	public static final String FILE_PATH = "C:\\Users\\Stepan Synoviat\\Documents\\task_IONIO\\";

	/**
	 * function shows all information about file
	 *
	 * @param filePath
	 * @throws FileNotFoundException
	 */
	public static void dispalyAll(String filePath) throws FileNotFoundException {
		File file = new File(filePath);
		if (file.exists()) {
			displayAll(file);
		} else {
			throw new FileNotFoundException(file.getPath());
		}
	}

	/**
	 * function shows all files from directory and they paths
	 *
	 * @param file
	 * @throws FileNotFoundException
	 */
	public static void displayAll(File file) throws FileNotFoundException {
		if (file.isDirectory()) {
			for (File child : file.listFiles()) {
				displayAll(child);
			}
		} else if (file.exists()) {
			logger.info(file.getPath());
		} else {
			throw new FileNotFoundException(file.getPath());
		}
	}

	/**
	 * function runs displayAll method
	 */
	public static void runDirectoryContent() {
		try {
			DirectoryContent.dispalyAll(FILE_PATH);
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
		}
	}
}
