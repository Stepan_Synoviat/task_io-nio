package task4_displayAllTheComments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Class CommentReader
 */
public class CommentReader {
	/**
	 * Logger initialization for CommentReader.class
	 */
	private static Logger logger = LogManager.getLogger(CommentReader.class);

	/**
	 * function shows all comments from source-code
	 */
	public static void showComments() {
		String string;
		boolean isCommented = false;
		try (BufferedReader br = new BufferedReader(
				new FileReader("ElectricalAppliance.java"))) {
			while ((string = br.readLine()) != null) {
				if (string.contains("//")) {
					logger.debug(string);
				} else if (string.contains("/*")) {
					logger.debug(string);
					isCommented = true;
				} else if (string.contains("*/")) {
					logger.debug(string);
					isCommented = false;
				} else if (isCommented) {
					logger.debug(string);
				}
			}
		} catch (IOException i) {
			logger.error(i.getMessage());
		}
	}
}

